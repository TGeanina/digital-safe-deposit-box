﻿using System;

public class User
{
    private string fName;
    private string lName;
    private string email;
    private string phone;
    private string username;
    private string password;
    private string secQ;
    private string secA;

    //generic
    public User()
    {
        fName = "Unkown";
        lName = "Unkown";
        email = "Unknown";
        phone = "Unknown";
        username = "Unknown";
        password = "Unknown";
        secQ = "Unknown";
        secA = "Unknown";
    }
    //specific
    public User(string fN, string lN, string eM, string p, string uN, string pass)
    {
        fName = fN;
        lName = lN;
        email = eM;
        phone = p;
        username = uN;
        password = pass;

    }

    //First name
    public string EditFName
    {
        get { return fName; }
        set { fName = value; }
    }
    //Last name
    public string EditLName
    {
        get { return lName; }
        set { lName = value; }
    }
    //Email
    public string EditEmail
    {
        get { return email; }
        set { email = value; }
    }
    //Phone number
    public string EditPhone
    {
        get { return phone; }
        set { phone = value; }
    }
    //Username
    public string EditUsername
    {
        get { return username; }
        set { username = value; }
    }
    //Password
    public string EditPassword
    {
        get { return password; }
        set { password = value; }
    }
}
