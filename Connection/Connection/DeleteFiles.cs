﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace Connection
{
    public partial class DeleteFiles : Form
    {
        public DeleteFiles()
        {
            InitializeComponent();
        }

        private void filesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.filesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet2);

        }

        private void DeleteFiles_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'database1DataSet.Files' table. You can move, or remove it, as needed.
            this.filesTableAdapter1.Fill(this.database1DataSet.Files);
            // TODO: This line of code loads data into the 'database1DataSet2.Files' table. You can move, or remove it, as needed.
            this.filesTableAdapter.Fill(this.database1DataSet2.Files);

        }

        private void delete_Click(object sender, EventArgs e)
        {
            using (SqlConnection con = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = D:\\CS305\\Connection\\Connection\\Database1.mdf; Integrated Security = True"))
            {
                //save the file to database
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "delete from dbo.Files where Id = " + this.textBox1.Text;
                SqlDataReader myReader;
                try
                {
                    con.Open();
                    myReader = cmd.ExecuteReader();
                    MessageBox.Show("Deleted");
                    while(myReader.Read())
                    {

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void back2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu menu = new MainMenu();
            textBox1.Text = "";
            menu.ShowDialog();
        }
    }
}
