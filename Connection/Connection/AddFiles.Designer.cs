﻿namespace Connection
{
    partial class AddFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addBox = new System.Windows.Forms.TextBox();
            this.fileLocation = new System.Windows.Forms.Label();
            this.openAdd = new System.Windows.Forms.Button();
            this.saveAdd = new System.Windows.Forms.Button();
            this.cancelAdd = new System.Windows.Forms.Button();
            this.back2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addBox
            // 
            this.addBox.Location = new System.Drawing.Point(12, 54);
            this.addBox.Name = "addBox";
            this.addBox.Size = new System.Drawing.Size(519, 22);
            this.addBox.TabIndex = 0;
            // 
            // fileLocation
            // 
            this.fileLocation.AutoSize = true;
            this.fileLocation.Location = new System.Drawing.Point(12, 25);
            this.fileLocation.Name = "fileLocation";
            this.fileLocation.Size = new System.Drawing.Size(88, 17);
            this.fileLocation.TabIndex = 1;
            this.fileLocation.Text = "File Location";
            // 
            // openAdd
            // 
            this.openAdd.Location = new System.Drawing.Point(12, 82);
            this.openAdd.Name = "openAdd";
            this.openAdd.Size = new System.Drawing.Size(87, 34);
            this.openAdd.TabIndex = 2;
            this.openAdd.Text = "Open";
            this.openAdd.UseVisualStyleBackColor = true;
            this.openAdd.Click += new System.EventHandler(this.openAdd_Click);
            // 
            // saveAdd
            // 
            this.saveAdd.Location = new System.Drawing.Point(127, 82);
            this.saveAdd.Name = "saveAdd";
            this.saveAdd.Size = new System.Drawing.Size(90, 34);
            this.saveAdd.TabIndex = 3;
            this.saveAdd.Text = "Save";
            this.saveAdd.UseVisualStyleBackColor = true;
            this.saveAdd.Click += new System.EventHandler(this.saveAdd_Click);
            // 
            // cancelAdd
            // 
            this.cancelAdd.Location = new System.Drawing.Point(248, 82);
            this.cancelAdd.Name = "cancelAdd";
            this.cancelAdd.Size = new System.Drawing.Size(92, 34);
            this.cancelAdd.TabIndex = 4;
            this.cancelAdd.Text = "Cancel";
            this.cancelAdd.UseVisualStyleBackColor = true;
            this.cancelAdd.Click += new System.EventHandler(this.cancelAdd_Click);
            // 
            // back2
            // 
            this.back2.Location = new System.Drawing.Point(387, 82);
            this.back2.Name = "back2";
            this.back2.Size = new System.Drawing.Size(144, 34);
            this.back2.TabIndex = 5;
            this.back2.Text = "Back to Main Menu";
            this.back2.UseVisualStyleBackColor = true;
            this.back2.Click += new System.EventHandler(this.back2_Click);
            // 
            // AddFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 179);
            this.Controls.Add(this.back2);
            this.Controls.Add(this.cancelAdd);
            this.Controls.Add(this.saveAdd);
            this.Controls.Add(this.openAdd);
            this.Controls.Add(this.fileLocation);
            this.Controls.Add(this.addBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddFiles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Files";
            this.Load += new System.EventHandler(this.AddFiles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox addBox;
        private System.Windows.Forms.Label fileLocation;
        private System.Windows.Forms.Button openAdd;
        private System.Windows.Forms.Button saveAdd;
        private System.Windows.Forms.Button cancelAdd;
        private System.Windows.Forms.Button back2;
    }
}