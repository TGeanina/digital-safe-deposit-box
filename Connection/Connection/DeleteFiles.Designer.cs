﻿namespace Connection
{
    partial class DeleteFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.database1DataSet2 = new Connection.Database1DataSet2();
            this.filesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.filesTableAdapter = new Connection.Database1DataSet2TableAdapters.FilesTableAdapter();
            this.tableAdapterManager = new Connection.Database1DataSet2TableAdapters.TableAdapterManager();
            this.delete = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchdescription = new System.Windows.Forms.Label();
            this.database1DataSet = new Connection.Database1DataSet();
            this.filesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.filesTableAdapter1 = new Connection.Database1DataSetTableAdapters.FilesTableAdapter();
            this.tableAdapterManager1 = new Connection.Database1DataSetTableAdapters.TableAdapterManager();
            this.filesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.back2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // database1DataSet2
            // 
            this.database1DataSet2.DataSetName = "Database1DataSet2";
            this.database1DataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // filesBindingSource
            // 
            this.filesBindingSource.DataMember = "Files";
            this.filesBindingSource.DataSource = this.database1DataSet2;
            // 
            // filesTableAdapter
            // 
            this.filesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.FilesTableAdapter = this.filesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Connection.Database1DataSet2TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(475, 80);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(136, 38);
            this.delete.TabIndex = 2;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(153, 88);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(308, 22);
            this.textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(148, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(463, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Input The Record You Want To Delete:";
            // 
            // searchdescription
            // 
            this.searchdescription.AutoSize = true;
            this.searchdescription.Location = new System.Drawing.Point(314, 37);
            this.searchdescription.Name = "searchdescription";
            this.searchdescription.Size = new System.Drawing.Size(139, 17);
            this.searchdescription.TabIndex = 6;
            this.searchdescription.Text = "You can delete by ID";
            // 
            // database1DataSet
            // 
            this.database1DataSet.DataSetName = "Database1DataSet";
            this.database1DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // filesBindingSource1
            // 
            this.filesBindingSource1.DataMember = "Files";
            this.filesBindingSource1.DataSource = this.database1DataSet;
            // 
            // filesTableAdapter1
            // 
            this.filesTableAdapter1.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.FilesTableAdapter = this.filesTableAdapter1;
            this.tableAdapterManager1.UpdateOrder = Connection.Database1DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // filesDataGridView
            // 
            this.filesDataGridView.AutoGenerateColumns = false;
            this.filesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.filesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.filesDataGridView.DataSource = this.filesBindingSource1;
            this.filesDataGridView.Location = new System.Drawing.Point(65, 124);
            this.filesDataGridView.Name = "filesDataGridView";
            this.filesDataGridView.RowTemplate.Height = 24;
            this.filesDataGridView.Size = new System.Drawing.Size(604, 168);
            this.filesDataGridView.TabIndex = 6;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "username";
            this.dataGridViewTextBoxColumn2.HeaderText = "username";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "file_name";
            this.dataGridViewTextBoxColumn3.HeaderText = "file_name";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // back2
            // 
            this.back2.Location = new System.Drawing.Point(317, 303);
            this.back2.Name = "back2";
            this.back2.Size = new System.Drawing.Size(144, 34);
            this.back2.TabIndex = 7;
            this.back2.Text = "Back to Main Menu";
            this.back2.UseVisualStyleBackColor = true;
            this.back2.Click += new System.EventHandler(this.back2_Click);
            // 
            // DeleteFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 349);
            this.Controls.Add(this.back2);
            this.Controls.Add(this.filesDataGridView);
            this.Controls.Add(this.searchdescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.delete);
            this.Name = "DeleteFiles";
            this.Text = "DeleteFiles";
            this.Load += new System.EventHandler(this.DeleteFiles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.database1DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Database1DataSet2 database1DataSet2;
        private System.Windows.Forms.BindingSource filesBindingSource;
        private Database1DataSet2TableAdapters.FilesTableAdapter filesTableAdapter;
        private Database1DataSet2TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label searchdescription;
        private Database1DataSet database1DataSet;
        private System.Windows.Forms.BindingSource filesBindingSource1;
        private Database1DataSetTableAdapters.FilesTableAdapter filesTableAdapter1;
        private Database1DataSetTableAdapters.TableAdapterManager tableAdapterManager1;
        private System.Windows.Forms.DataGridView filesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button back2;
    }
}