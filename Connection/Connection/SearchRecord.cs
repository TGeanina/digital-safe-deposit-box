﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Connection
{
    public partial class SearchRecord : Form
    {
        public SearchRecord()
        {
            InitializeComponent();
        }

        private void filesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.filesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.database1DataSet);

        }

        private void SearchRecord_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'database1DataSet.Files' table. You can move, or remove it, as needed.
            this.filesTableAdapter.Fill(this.database1DataSet.Files);

        }

        private void Search_Click(object sender, EventArgs e)
        {
            String search = txtSearch.Text;
            this.filesTableAdapter.FillBySearch(this.database1DataSet.Files, search, search);
        }

        private void back_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu menu = new MainMenu();
            txtSearch.Text = "";
            menu.ShowDialog();
        }
    }
}
