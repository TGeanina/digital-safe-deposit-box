﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Connection
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void AddFiles_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddFiles addFiles = new AddFiles();
            addFiles.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            SearchRecord search = new SearchRecord();
            search.ShowDialog();
        }

        private void delete_Click(object sender, EventArgs e)
        {
            this.Hide();
            DeleteFiles delete = new DeleteFiles();
            delete.ShowDialog();
        }
    }
}
