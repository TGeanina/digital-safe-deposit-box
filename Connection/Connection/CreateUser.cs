﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Connection
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public string conString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\CS305\\Connection\\Connection\\Database1.mdf;Integrated Security=True";
        private void button1_Click(object sender, EventArgs e)
        {
            String firstName = textFName.Text;
            String lastName = textLName.Text;
            String email = textEmail.Text;
            String number = textPhone.Text;
            String username = textUsername.Text;
            String password = textPassword.Text;
            String rePassword = textRePassword.Text;
            Boolean validPassword = false;
            // Regex regex = new Regex(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{14,}$");
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMaxChars = new Regex(@".{14,}");
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
            
            if (hasLowerChar.IsMatch(textPassword.Text)&&hasSymbols.IsMatch(textPassword.Text)&& hasNumber.IsMatch(textPassword.Text)&& hasMaxChars.IsMatch(textPassword.Text)&& hasUpperChar.IsMatch(textPassword.Text))
            {
                validPassword = true;
            }
            //Console.WriteLine(hasLowerChar.IsMatch(textPassword.Text) +","+ hasSymbols.IsMatch(textPassword.Text) +","+ hasNumber.IsMatch(textPassword.Text) +","+ hasMaxChars.IsMatch(textPassword.Text) +","+ hasUpperChar.IsMatch(textPassword.Text));
            if (validPassword==true && password.Equals(rePassword)&&(!firstName.All(char.IsWhiteSpace)&&!lastName.All(char.IsWhiteSpace)&&!username.All(char.IsWhiteSpace)&&!password.All(char.IsWhiteSpace)))
            {
                User user = new User(firstName, lastName, email, number, username, password);

                SqlConnection con = new SqlConnection(conString);
                con.Open();
                if (con.State == ConnectionState.Open)
                {
                    
                    SqlCommand check_User_Name = new SqlCommand("SELECT COUNT(*) FROM [User] WHERE ([username] = @username)", con);
                    check_User_Name.Parameters.AddWithValue("@username", textUsername.Text);
                    int UserExist = (int)check_User_Name.ExecuteScalar();

                    if (UserExist > 0)
                    {
                        //username exists
                        message.Text = "Username: "+ textUsername.Text+" already in use. Please create a new username.";
                        message.Visible = true;
                        textUsername.Text = "";
                    }
                    else
                    {
                        //Username doesn't exist.
                        String query = "INSERT INTO [User](firstName,lastName,email,phone,username,password,admin) VALUES(@firstName,@lastName,@email,@phone,@username,@password,@admin)";
                        SqlCommand cmd = new SqlCommand(query, con);
                        SqlCommand comm = new SqlCommand("SELECT COUNT(*) FROM [User]", con);
                        Int32 count = (Int32)comm.ExecuteScalar();
                        Console.Out.WriteLine(count);
                        //cmd.Parameters.AddWithValue("@firstName,@lastName,@email,@phone,@username,@password", firstName+","+lastName+","+email+","+number+","+username+","+password);
                        cmd.Parameters.AddWithValue("@firstName", firstName);
                        cmd.Parameters.AddWithValue("@lastName", lastName);
                        cmd.Parameters.AddWithValue("@email", email);
                        cmd.Parameters.AddWithValue("@phone", number);
                        cmd.Parameters.AddWithValue("@username", username);
                        cmd.Parameters.AddWithValue("@password", password);
                        if (count == 0)
                            cmd.Parameters.AddWithValue("@admin", 1);
                        else
                            cmd.Parameters.AddWithValue("@admin", 0);
                        cmd.ExecuteNonQuery();
                        message.Text = "";
                        message.Visible = true;
                        textFName.Text = "";
                        textLName.Text = "";
                        textEmail.Text = "";
                        textPhone.Text = "";
                        textUsername.Text = "";
                        textPassword.Text = "";
                        textRePassword.Text = "";
                        this.Hide();
                        LoginScreen login = new LoginScreen();
                        this.Close();
                    }
                }
                else
                {
                    message.Text = "Could not connect to the database try again later";
                    message.Visible = true;
                    textFName.Text = "";
                    textLName.Text = "";
                    textEmail.Text = "";
                    textPhone.Text = "";
                    textUsername.Text = "";
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }
                
            }
            else
            {
                if (!hasLowerChar.IsMatch(textPassword.Text))
                {
                    message.Text = "Password should contain At least one lower case letter";
                    message.Visible = true;
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }
                else if (!hasUpperChar.IsMatch(textPassword.Text))
                {
                    message.Text = "Password should contain At least one upper case letter";
                    message.Visible = true;
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }
                else if (!hasMaxChars.IsMatch(textPassword.Text))
                {
                    message.Text = "Password should not be less than 14 characters";
                    message.Visible = true;
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }
                else if (!hasNumber.IsMatch(textPassword.Text))
                {
                    message.Text = "Password should contain At least one numeric value";
                    message.Visible = true;
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }

                else if (!hasSymbols.IsMatch(textPassword.Text))
                {
                    message.Text = "Password should contain At least one special case character";
                    message.Visible = true;
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }
                else if (!password.Equals(rePassword))
                {
                    message.Text = "Passwords do not match.";
                    message.Visible = true;
                    textPassword.Text = "";
                    textRePassword.Text = "";
                }
                
                else
                {
                    message.Text = "First name, last name, username, and password values may not contain only whitspace.";
                    message.Visible = true;
                }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
