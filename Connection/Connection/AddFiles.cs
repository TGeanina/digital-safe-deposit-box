﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace Connection
{
    
    public partial class AddFiles : Form
    {
        LoginScreen ls=new LoginScreen();
        OpenFileDialog opfile = new OpenFileDialog();
        String filename;
        String full_file_name;
        public AddFiles()
        {
            InitializeComponent();
        }

        private void openAdd_Click(object sender, EventArgs e)
        {
            //open a dialog box for us to select our file for upload
            if(opfile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                addBox.Text = opfile.FileName;
            }



        }

        private void cancelAdd_Click(object sender, EventArgs e)
        {
            opfile.FileName = null;
            addBox.Text = "";


        }

        private void saveAdd_Click(object sender, EventArgs e)
        {
            full_file_name = addBox.Text;
            String username;
            username = ls.MyVal;
            Console.WriteLine(username);
            String[] words=full_file_name.Split('\\');
            filename = words[words.Length-1];
            using (SqlConnection con = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = D:\\CS305\\Connection\\Connection\\Database1.mdf; Integrated Security = True"))
            {
                //save the file to database
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "insert into dbo.Files (username, file_name, content) values (@username,@file_name,@file)";
                if (username != null)
                {
                    cmd.Parameters.AddWithValue("username", username);
                }
                else
                {
                    cmd.Parameters.AddWithValue("username", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("file_name", filename);
                cmd.Parameters.AddWithValue("file", SqlDbType.VarBinary).Value = File.ReadAllBytes(addBox.Text);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("File Saved");
                addBox.Clear();
            }

        }

        private void AddFiles_Load(object sender, EventArgs e)
        {
           
        }

        private void back2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainMenu menu = new MainMenu();
            addBox.Text = "";
            menu.ShowDialog();
        }
    }
}
